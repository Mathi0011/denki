from PIL import Image, ImageDraw, ImageChops

size = 1048

im = Image.new('RGB', (size, size), (255, 255, 255))
draw = ImageDraw.Draw(im)

draw.polygon((
    (size / 2, size * 0.25),
    (size * 0.75, size / 2),
    (size / 2, size * 0.75),
    (size * 0.25, size / 2)),
    outline='black'
)

draw.polygon((
    (size / 2, size / 2),
    (size * 0.8, size * 0.8),
    (size * 0.2, size * 0.8)),
    outline='black'
)

draw.ellipse((
    (size * 0.4, size * 0.8),
    (size * 0.6, size)),
    outline='black'
)

im_rotated_ellipse = Image.new('RGBA', (size, size))
im_rotated_ellipse_draw = ImageDraw.Draw(im_rotated_ellipse)
im_rotated_ellipse_draw.ellipse((
    (size * 0.3, size * 0.3),
    (size * 0.6, size * 0.9)),
    outline='black'
)
im_rotated_ellipse = im_rotated_ellipse.rotate(-45)
im_rotated_ellipse = ImageChops.offset(im_rotated_ellipse, - int(size / 12), - int(size / 4))
im.paste(im_rotated_ellipse, (0, 0), im_rotated_ellipse)

draw.polygon((
    (size * 0.45, size * 0.25),
    (size * 0.75, size * 0.55 + 1),
    (size * 0.9 + 1, size * 0.4),
    (size * 0.6 + 1, size * 0.1)),
    outline='black'
)

draw.polygon((
    (size / 2, size * 0.8),
    (size * 0.6, size * 0.7),
    (size / 2, size * 0.6),
    (size * 0.4, size * 0.7)),
    outline='black'
)

draw.rectangle((
    (size * 0.07, size * 0.522),
    (size * 0.2, size * 0.938)),
    outline='black'
)

draw.ellipse((
    (size * 0.07, size * 0.87),
    (size * 0.2, size)),
    outline='black'
)

im.show()
im.save("code_result.jpg")
