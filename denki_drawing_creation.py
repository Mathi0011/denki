from numpy.random import choice

shapes = ['Rechteck', 'Quadrat', 'Kreis', 'Ellipse', 'Stern', 'Dreieck']
positions = ['rechts von', 'links von', 'über', 'unter', 'in der Mitte von']
rotations = ['', 'um 45° gedreht']
rotations_probability_distribution = (0.75, 0.25)

drawn_shapes = [(choice(shapes), '', choice(rotations, size=1, p=rotations_probability_distribution)[0], None)]
for i in range(1, 8):
    drawn_shapes.append(
        (choice(shapes),
         choice(positions),
         choice(rotations, 1, choice(rotations, size=1, p=rotations_probability_distribution))[0],
         choice(i))
    )

print("Schritt {}: Zeichne ein(en) {} {}".format(0, drawn_shapes[0][0], drawn_shapes[0][2]))
for i, shape in enumerate(drawn_shapes[1:]):
    print("Schritt {}: Zeichne ein(en) {} {} dem in Schritt {} gezeichnetem {} {}".format(
        i + 1,
        shape[0],
        shape[1],
        shape[3],
        drawn_shapes[shape[3]][0],
        shape[2])
    )
